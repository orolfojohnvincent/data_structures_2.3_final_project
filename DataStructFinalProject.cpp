#include <iostream>
#include <cstdlib>
#include <conio.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>

using namespace std;

static int maxi=0, stkr=0;
class List{
	private:
		typedef struct node{
			string title;
			string singer;
			string genre;
			int lbl;
			node* next;
		}* nodePtr;
		
		nodePtr head;
		nodePtr curr;
		nodePtr temp;
		nodePtr tail;
		
	
	public:
		List();
		void AddNode(string addTitle, string addSinger,string genre, int hub);
		void Push(string addTitle, string addSinger,string genre, int hub);
		void Dequeue();
		void Pop();
		void DeleteNode(int index);
		void PrintList();
		void PrintPlayList();
		void EditNode(int index, string newMusic);
		int FindData(string music);
		void Playing(int index);
		void PlayingList(int index);
		void sort();
		bool IsEmpty();
		
};

List::List(){
	head = NULL;
	curr = NULL;
	temp = NULL;
	tail = NULL;
}

bool List::IsEmpty(){
	curr=head;
	if(head!=NULL){
		//cout<<"The stack is not empty\n";
		return false;
	}
	return true;
}
void List::AddNode(string addTitle, string addSinger, string genre, int hub){
	nodePtr n = new node;
	n->next = NULL;
	n->title = addTitle;
	n->singer = addSinger;
	n->genre = genre;
	n->lbl = hub;
	if(head != NULL){
		curr = head;
		while (curr->next != NULL){
			curr = curr->next;
		}
		curr->next = n;
		tail = n;
		maxi++;
	}
	else{
		head = n;
		tail = n;
		maxi++;
	}
}

void List::Push(string addTitle, string addSinger,string genre, int hub){
	nodePtr n = new node;
	n->next = NULL;
	n->title = addTitle;
	n->singer = addSinger;
	n->genre = genre;
	n->lbl = hub;
	if(tail != NULL){
		curr = head;
		while (curr->next != NULL){
			curr = curr->next;
		}
		curr->next = n;
		tail = n;
		stkr++;
	}
	else{
		head = n;
		tail = n;
		stkr++;
	}
}
void List::Dequeue(){
	if(head==NULL){
		//cout<<"Playlist is empty!\n";
	}
	else if(head->next == NULL){
		head = NULL;
		//cout<<"Playlist is now empty!\n";
		stkr--;
	}
	else{
		curr = head;
		head = head->next;
		curr->next = NULL;
		cout<<"Successfully Dequeued!\n";
		stkr--;
	}
}
void List::Pop(){
	nodePtr delPtr = NULL;
	temp = head;
	curr = head;
	int i=0;
	if(head != NULL){
		while (curr->next != NULL&&i!=stkr-1){
			temp = curr;
			curr = curr->next;
			i++;
		}
	}
	if(curr==tail){	
		//cout<<tail->title<<" by "<<tail->singer<<" ("<<tail->genre<<") is popped-out of the playlist\n";
		cout<<"Successfully Popped!\n";	
		system("pause");
		delPtr = tail;
		tail = temp;
		stkr--;
		//maxi--;	
	}
	delete delPtr;
}

void List::DeleteNode(int index){
	nodePtr delPtr = NULL;
	temp = head;
	curr = head;
	int i=0;
	while (curr != NULL &&i!=index-1){
		temp = curr;
		curr = curr->next;
		i++;
	}
	if (curr == NULL){
		cout/*<<delMusic*/<<" was not in the List\n";
	}
	else if (curr==head){
		delPtr=head;
		head=head->next;
		maxi--;
	}
	else if(curr==tail){	
		delPtr = tail;
		tail = temp;
		maxi--;	
	}
	else{
		delPtr = curr;
		curr = curr->next;
		temp->next = curr;
		maxi--;
	}
}
void List::PrintPlayList(){
	curr = head;
	int x = 1;
	//temp = curr->next;
	if(stkr==0){
		cout<<"Playlist is empty!\n";
	}
	while (x!=stkr+1){
		/*if(x=900){
			cout<<"Title: "<<curr->title<<"||"<<curr->genre<<"\n";
			cout<<"Singer: "<<curr->singer<<"\n";
			x++;
		curr = curr->next;
		}*/
		cout<<"("<<x<<")\n";
		cout<<"Title: "<<curr->title<<"||"<<curr->genre<<"\n";
		cout<<"Singer: "<<curr->singer<<"\n";
		x++;
		curr = curr->next;
	}
}

void List::PrintList(){
	curr = head;
	int x = 1;
	//temp = curr->next;
	while (x!=maxi+1){
		/*if(x=900){
			cout<<"Title: "<<curr->title<<"||"<<curr->genre<<"\n";
			cout<<"Singer: "<<curr->singer<<"\n";
			x++;
		curr = curr->next;
		}*/
		cout<<"("<<x<<")\n";
		cout<<"Title: "<<curr->title<<"||"<<curr->genre<<"\n";
		cout<<"Singer: "<<curr->singer<<"\n";
		x++;
		curr = curr->next;
	}
}
void List::sort(){
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	int i = 1, x=1;
	for(i;i<=13;i++){
		curr=head;
		while(curr->next!=NULL){
			if(curr->lbl==i){
			SetConsoleTextAttribute(hConsole, i); 
			cout<<"("<<x<<")\n";
			cout<<"Title: "<<curr->title<<"||"<<curr->genre<<"\n";
			cout<<"Singer: "<<curr->singer<<"\n";
			x++;
			}
			curr=curr->next;
		}		
	}
	cout<<"("<<x<<")\n";
	cout<<"Title: "<<curr->title<<"||"<<curr->genre<<"\n";
	cout<<"Singer: "<<curr->singer<<"\n";
	/*while(i!=maxi+1){
		//curr=head;
		if(curr->lbl==1){
			SetConsoleTextAttribute(hConsole, 1); 
			cout<<"("<<x<<")\n";
			cout<<"Title: "<<curr->title<<"||"<<curr->genre<<"\n";
			cout<<"Singer: "<<curr->singer<<"\n";
			x++;
			i++;
			curr=curr->next;
		}
		else if(curr->lbl==2){
			SetConsoleTextAttribute(hConsole, 2); 
			cout<<"("<<x<<")\n";
			cout<<"Title: "<<curr->title<<"||"<<curr->genre<<"\n";
			cout<<"Singer: "<<curr->singer<<"\n";
			x++;
			i++;
			curr=curr->next;
		}
		else if(curr->lbl==3){
			SetConsoleTextAttribute(hConsole, 3); 
			cout<<"("<<x<<")\n";
			cout<<"Title: "<<curr->title<<"||"<<curr->genre<<"\n";
			cout<<"Singer: "<<curr->singer<<"\n";
			x++;
			i++;
			curr=curr->next;
		}
		else if(curr->lbl==4){
			SetConsoleTextAttribute(hConsole, 4); 
			cout<<"("<<x<<")\n";
			cout<<"Title: "<<curr->title<<"||"<<curr->genre<<"\n";
			cout<<"Singer: "<<curr->singer<<"\n";
			x++;
			i++;
			curr=curr->next;
		}
		else if(curr->lbl==5){
			SetConsoleTextAttribute(hConsole, 5); 
			cout<<"("<<x<<")\n";
			cout<<"Title: "<<curr->title<<"||"<<curr->genre<<"\n";
			cout<<"Singer: "<<curr->singer<<"\n";
			x++;
			i++;
			curr=curr->next;
		}
		else if(curr->lbl==6){
			SetConsoleTextAttribute(hConsole, 6); 
			cout<<"("<<x<<")\n";
			cout<<"Title: "<<curr->title<<"||"<<curr->genre<<"\n";
			cout<<"Singer: "<<curr->singer<<"\n";
			x++;
			i++;
			curr=curr->next;
		}
		else if(curr->lbl==7){
			SetConsoleTextAttribute(hConsole, 7); 
			cout<<"("<<x<<")\n";
			cout<<"Title: "<<curr->title<<"||"<<curr->genre<<"\n";
			cout<<"Singer: "<<curr->singer<<"\n";
			x++;
			i++;
			curr=curr->next;
		}
		else if(curr->lbl==8){
			SetConsoleTextAttribute(hConsole, 8); 
			cout<<"("<<x<<")\n";
			cout<<"Title: "<<curr->title<<"||"<<curr->genre<<"\n";
			cout<<"Singer: "<<curr->singer<<"\n";
			x++;
			i++;
			curr=curr->next;
		}
		else if(curr->lbl==9){
			SetConsoleTextAttribute(hConsole, 9); 
			cout<<"("<<x<<")\n";
			cout<<"Title: "<<curr->title<<"||"<<curr->genre<<"\n";
			cout<<"Singer: "<<curr->singer<<"\n";
			x++;
			i++;
			curr=curr->next;
		}
		else if(curr->lbl==10){
			SetConsoleTextAttribute(hConsole, 10); 
			cout<<"("<<x<<")\n";
			cout<<"Title: "<<curr->title<<"||"<<curr->genre<<"\n";
			cout<<"Singer: "<<curr->singer<<"\n";
			x++;
			i++;
			curr=curr->next;
		}
		else if(curr->lbl==11){
			SetConsoleTextAttribute(hConsole, 11); 
			cout<<"("<<x<<")\n";
			cout<<"Title: "<<curr->title<<"||"<<curr->genre<<"\n";
			cout<<"Singer: "<<curr->singer<<"\n";
			x++;
			i++;
			curr=curr->next;
		}
		else if(curr->lbl==12){
			SetConsoleTextAttribute(hConsole, 12); 
			cout<<"("<<x<<")\n";
			cout<<"Title: "<<curr->title<<"||"<<curr->genre<<"\n";
			cout<<"Singer: "<<curr->singer<<"\n";
			x++;
			i++;
			curr=curr->next;
		}
		else if(curr->lbl==13){
			SetConsoleTextAttribute(hConsole, 13); 
			cout<<"("<<x<<")\n";
			cout<<"Title: "<<curr->title<<"||"<<curr->genre<<"\n";
			cout<<"Singer: "<<curr->singer<<"\n";
			x++;
			i++;
			curr=curr->next;
		}
		
	}*/
}


//at a specified index
void List::EditNode(int index, string newMusic){
	curr = head;
	for (int currIndex = 1; currIndex <= index; currIndex++){
		curr = curr->next;
	}
	if (curr){
		curr->title = newMusic;
		//curr->singer = newSinger;
	}
}

int List::FindData(string music){
	curr = head;
	int currIndex = 1;
	while (curr && curr->title != music){
		curr = curr->next;
		currIndex++;
	}
	if (curr){
		//cout<<music<<" is in the "<<currIndex<<"position"<<"\n";
		return currIndex;
	}
}
void List::PlayingList(int index){
	nodePtr n;
	curr = head;
	int currIndex = 1/*, maxicounter=1*/;
	/*while(curr&&curr->next!=NULL){
		curr=curr->next;
		maxicounter++;
	}*/
	if(stkr==1&&index==1){
		//curr= head;
		cout<<"Previous: None\n";
		cout<<"Playing: "<<curr->title<<" by "<<curr->singer<<" ("<<curr->genre<<")\n";
		cout<<"Next: None\n";
	}
	else if(index==1){
		curr=head;
		cout<<"Previous: None\n";
		while(currIndex!=index+1){
			temp = curr;
			curr=curr->next;
			currIndex++;
		}
		cout<<"Playing: "<<temp->title<<" by "<<temp->singer<<" ("<<curr->genre<<")"<<endl;
		cout<<"Next: "<<curr->title<<" by "<<curr->singer<<" ("<<curr->genre<<")"<<endl;
	}
	else if(index==stkr){
		curr=head;
		while (currIndex != index&&curr->next!=NULL){
			temp = curr;
			curr = curr->next;
			currIndex++;
		}
		cout<<"Previous: "<<temp->title<<" by "<<temp->singer<<" ("<<temp->genre<<")"<<endl;
		cout<<"Playing: "<<curr->title<<" by "<<curr->singer<<" ("<<curr->genre<<")"<<endl;
		cout<<"Next: None\n";
	}
	else{
		curr=head;
		while(currIndex!=index&&curr->next!=NULL){
			temp=curr;
			curr=curr->next;
			n=curr->next;
			currIndex++;
		}
		cout<<"Previous: "<<temp->title<<" by "<<temp->singer<<" ("<<curr->genre<<")"<<endl;
		cout<<"Playing: "<<curr->title<<" by "<<curr->singer<<" ("<<curr->genre<<")"<<endl;
		cout<<"Next: "<<n->title<<" by "<<n->singer<<" ("<<curr->genre<<")"<<endl;
	}
}
void List::Playing(int index){
	nodePtr n;
	curr = head;
	int currIndex = 1/*, maxicounter=1*/;
	/*while(curr&&curr->next!=NULL){
		curr=curr->next;
		maxicounter++;
	}*/
	if(index==1){
		curr=head;
		cout<<"Previous: None\n";
		while(currIndex!=index+1){
			temp = curr;
			curr=curr->next;
			currIndex++;
		}
		cout<<"Playing: "<<temp->title<<" by "<<temp->singer<<" ("<<curr->genre<<")"<<endl;
		cout<<"Next: "<<curr->title<<" by "<<curr->singer<<" ("<<curr->genre<<")"<<endl;
	}
	else if(index==maxi){
		curr=head;
		while (currIndex != index&&curr->next!=NULL){
			temp = curr;
			curr = curr->next;
			currIndex++;
		}
		cout<<"Previous: "<<temp->title<<" by "<<temp->singer<<" ("<<temp->genre<<")"<<endl;
		cout<<"Playing: "<<curr->title<<" by "<<curr->singer<<" ("<<curr->genre<<")"<<endl;
		cout<<"Next: None\n";
	}
	else{
		curr=head;
		while(currIndex!=index&&curr->next!=NULL){
			temp=curr;
			curr=curr->next;
			n=curr->next;
			currIndex++;
		}
		cout<<"Previous: "<<temp->title<<" by "<<temp->singer<<" ("<<curr->genre<<")"<<endl;
		cout<<"Playing: "<<curr->title<<" by "<<curr->singer<<" ("<<curr->genre<<")"<<endl;
		cout<<"Next: "<<n->title<<" by "<<n->singer<<" ("<<curr->genre<<")"<<endl;
	}
}

bool Close(){
	return 0;
}
void intro();
List yey, wow;
void introdin(){
	string singer, mtitle, mtite;
	system("cls");
	cout<<"=======================================================PLAYLIST==================================================\n";
	int pck;
			cout<<"1. View Playlist\t 2. Create Playlist\t 3. Delete from Playlist\t 4. Play Next Song\t 5. Back\n";
			cin>>pck;
			switch(pck){
				case 1:{
					bool op;
					op = wow.IsEmpty();
					if(op==true){
						cout<<"Playlist has no content!\n";
						system("pause");
						introdin();
					}
					else{
						wow.PrintPlayList();
						if(stkr!=0){
						
						cout<<"Choose a number to play: ";
						cin>>pck;
						cin.ignore(1,'\n');
						if(pck>stkr){
							cout<<"     INVALID INPUT! 	\n";
							system("pause");
							introdin();
						}
						else wow.PlayingList(pck);
					}
						system("pause");		
						system("cls");
						introdin();
					}
					break;
				}
				case 2:{
					int cgenre;
					string mtite, singger, hatdog;
					yey.PrintList();
					cout<<"\n";
					cout<<"Pick a music from the list\n";
					cout<<"======================================================"<<"\n";
					getline(cin, hatdog);
					system("pause");
					cout<<"Enter music title to be added(case-sensitive): "<<"\n";
					getline(cin, mtite);
					cout<<"Enter singer(Case-sensitive): "<<"\n";
					getline(cin, singer);
					cout<<"Genre of music: "<<"\n"<<" 1. Acoustic\n 2. Alternative\n 3. Country\n 4. Emo\n 5. Hiphop\n 6. Indie\n 7. Metal\n 8. Pop\n 9. Pop-Punk\n 10. Pop-Rock\n 11. Reggae\n 12. R&B\n 13. Rock\n ";
					cin>>cgenre;
					switch(cgenre){
					case 1:{
						wow.Push(mtite, singer,"ACOUSTIC",1);
						cout<<"Successfully Added!\n";
						break;
					}
					case 2:{
						wow.Push(mtite, singer,"ALTERNATIVE",2);
						cout<<"Successfully Added!\n";
						break;
					}
					case 3:{
						wow.Push(mtite, singer,"COUNTRY",3);
						cout<<"Successfully Added!\n";
						break;
					}
					case 4:{
						wow.Push(mtite, singer,"EMO",4);
						cout<<"Successfully Added!\n";
						break;
					}
					case 5:{
						wow.Push(mtite, singer,"HIPHOP",5);
						cout<<"Successfully Added!\n";
						break;
					}
					case 6:{
						wow.Push(mtite, singer,"INDIE",6);
						cout<<"Successfully Added!\n";
						break;
					}
					case 7:{
						wow.Push(mtite, singer,"METAL",7);
						cout<<"Successfully Added!\n";		
						break;
					}
					case 8:{
						wow.Push(mtite, singer,"POP",8);
						break;
					}
					case 9:{
						wow.Push(mtite, singer,"POP-PUNK",9);
						cout<<"Successfully Added!\n";
						break;
					}
					case 10:{
						wow.Push(mtite, singer,"POP-ROCK",10);
						cout<<"Successfully Added!\n";
						break;
					}
					case 11:{
						wow.Push(mtite, singer,"REGGAE",11);
						cout<<"Successfully Added!\n";
						break;
					}
					case 12:{
						wow.Push(mtite, singer,"R&B",12);
						cout<<"Successfully Added!\n";
						break;
					}
					case 13:{
						wow.Push(mtite, singer,"ROCK",13);
						cout<<"Successfully Added!\n";
						break;
					}
					default:{
						cout<<"INVALID INPUT!\n";
						break;
					}
				}
					system("cls");
					introdin();
					break;
				}
				case 3:{
					if(stkr!=0) wow.Pop();
					else {
						cout<<"Nothing to delete.\n";
						system("pause");
					}
					system("cls");
					introdin();
					break;
				}
				case 5:{
					intro();
					break;
				}
				case 4:{
					if(stkr==0) cout<<"Nothing to play.\n";
					else if(stkr==1){
						wow.Dequeue();
						cout<<"Nothing in Queue\n";
					}
					else {
						wow.Dequeue();
						wow.PlayingList(1);
					}
					system("pause");
					system("cls");
					introdin();
					break;
				}
				default:{
					cout<<"INVALID INPUT!\n";
					system("pause");
					introdin();
					break;
				}
						
		}
}

void intro(){
	int c;
	int cgenre;
	string mtitle, singer;
	system("cls");
	cout<<" 1. Add Music\n 2. Display Playlist\n 3. Edit Music Title\n 4. Delete Music\n 5. Sort by Genre\n 6. Create Playlist\n 7. Exit\n ";
	cin>>c;
	cin.ignore(1,'\n');
	if(c>8||c<1){
		cout<<"INVALID INPUT!\n\n";
		intro();
	}
	switch (c){
		case 1:{ 
			cout<<"Enter music title: "<<"\n";
			getline(cin, mtitle);
			cout<<"Enter singer: "<<"\n";
			getline(cin, singer);
			cout<<"Genre of music: "<<"\n"<<" 1. Acoustic\n 2. Alternative\n 3. Country\n 4. Emo\n 5. Hiphop\n 6. Indie\n 7. Metal\n 8. Pop\n 9. Pop-Punk\n 10. Pop-Rock\n 11. Reggae\n 12. R&B\n 13. Rock\n ";
			cin>>cgenre;
			switch(cgenre){
				case 1:{
					yey.AddNode(mtitle, singer,"ACOUSTIC",1);
					cout<<"Successfully Added!\n";
					break;
				}
				case 2:{
					yey.AddNode(mtitle, singer,"ALTERNATIVE",2);
					cout<<"Successfully Added!\n";
					break;
				}
				case 3:{
					yey.AddNode(mtitle, singer,"COUNTRY",3);
					cout<<"Successfully Added!\n";
					break;
				}
				case 4:{
					yey.AddNode(mtitle, singer,"EMO",4);
					cout<<"Successfully Added!\n";
					break;
				}
				case 5:{
					yey.AddNode(mtitle, singer,"HIPHOP",5);
					cout<<"Successfully Added!\n";
					break;
				}
				case 6:{
					yey.AddNode(mtitle, singer,"INDIE",6);
					cout<<"Successfully Added!\n";
					break;
				}
				case 7:{
					yey.AddNode(mtitle, singer,"METAL",7);
					cout<<"Successfully Added!\n";		
					break;
				}
				case 8:{
					yey.AddNode(mtitle, singer,"POP",8);
					break;
				}
				case 9:{
					yey.AddNode(mtitle, singer,"POP-PUNK",9);
					cout<<"Successfully Added!\n";
					break;
				}
				case 10:{
					yey.AddNode(mtitle, singer,"POP-ROCK",10);
					cout<<"Successfully Added!\n";
					break;
				}
				case 11:{
					yey.AddNode(mtitle, singer,"REGGAE",11);
					cout<<"Successfully Added!\n";
					break;
				}
				case 12:{
					yey.AddNode(mtitle, singer,"R&B",12);
					cout<<"Successfully Added!\n";
					break;
				}
				case 13:{
					yey.AddNode(mtitle, singer,"ROCK",13);
					cout<<"Successfully Added!\n";
					break;
				}
				default:{
					cout<<"INVALID INPUT!\n";
					break;
				}
			}
			system("pause");
			intro();
			break;
		}
		case 2:{
			int pick,random;
			yey.PrintList();
			cout<<" 1. Choose a number to play\t2. Play Random Song\n";
			cin>>pick;
			cin.ignore(1,'\n');
			if(pick>2){
				cout<<"     INVALID INPUT! 	\n";
			}
			if(pick==1){
				cout<<"Song Choice:\t";
				cin>>random;
				if(random>maxi){
					cout<<"	INVALID INPUT!	\n";
					system("pause");
				}
				else yey.Playing(random);
			}
			else if(pick==2){
				random = rand()%1500+1;
				cout<<" Random song: \n";
				yey.Playing(random);
			}
			system("pause");		
			intro();
			break;
		}
		case 3:{
			//edit
			string newTitle;
			int edit;
			yey.PrintList();
			cout<<"Enter the new title\n";
			getline(cin, newTitle);
			cout<<"Enter the number of the song to edit: \n";
			cin>>edit;
			//cout<<"Enter the new singer(thenpressEnter)\n";
			//getline(cin, newSinger);
			system("pause");
			yey.EditNode(edit-1, newTitle);
			intro();
			break;
		}
		case 4:{
			//delete
			yey.PrintList();
			cout<<"Enter Music Number to Delete:\n";
			//getline(cin, mtitle);
			cin>>c;
			yey.DeleteNode(c);
			intro();
			break;
		}
		case 5:{
			yey.sort();
			//yey.PrintList();
			//cout<<"                               GOODBYE.... ";
			//Close();
			system("pause");
			intro();
			break;
		}
		case 6:{
			introdin();
			break;
		}
		case 7:{
			cout<<"                               GOODBYE....\n ";
			Close();
			break;
		}
}
}

int main(){
	yey.AddNode("All of a Sudden","Matt Monro","COUNTRY",3);
	yey.AddNode("Kay Tagal", "Mark Carpio", "EMO", 4);
	yey.AddNode("Walang Kwenta", "Bassilyo", "HIPHOP", 5);
	yey.AddNode("Dungaw", "Gloc-9", "HIPHOP", 5);
	yey.AddNode("Wonderful Sound", "Tom Jones", "COUNTRY", 3);
	yey.AddNode("Crazy", "Kenny Rogers","COUNTRY",3); 
	yey.AddNode("Pulang Manok", "Unknown","POP",8);
	yey.AddNode("Goodbye","Air Supply","POP", 8);
	yey.AddNode("Istokwa", "Lexus", "EMO", 4);   
	yey.AddNode("Dizzy On The Comedown ","Turnover","ALTERNATIVE",2);
	yey.AddNode("Cutting My Fingers Off ","Turnover","ALTERNATIVE",2);
	yey.AddNode("Take My Head ","Turnover","ALTERNATIVE",2);
	yey.AddNode("Diazepam ","Turnover","ALTERNATIVE",2);
	yey.AddNode("New Scream ","Turnover","ALTERNATIVE",2);
	yey.AddNode("Threshold ","Turnover","ALTERNATIVE",2);
	yey.AddNode("Humming ","Turnover","ALTERNATIVE",2);
	yey.AddNode("Like Slowly Disappearing ","Turnover","ALTERNATIVE",2);
	yey.AddNode("I Would Hate You If I Could ","Turnover","ALTERNATIVE",2);
	yey.AddNode("Intrapersonal ","Turnover","ALTERNATIVE",2);
	//FOREST HILLS DRIVE 2014 13
	yey.AddNode("Intro ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("January 25th ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("Wet Dreamz ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("03' Adolescence' ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("A tale of two Citiez ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("FireSquad ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("St Tropez ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("Hello ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("G.O.M.D ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("No Role Modelz ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("Apparently ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("Love Yourz ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("Note To Self ", "JCOLE", "HIPHOP", 5);
	// WE ARE NOT YOUR KIND 14
	yey.AddNode("Nero Forte ", "SLIPKNOT", "METAL", 7);
	yey.AddNode("Spiders ", "SLIPKNOT", "METAL", 7);	
	yey.AddNode("Insert Coin ", "SLIPKNOT", "METAL", 7);
	yey.AddNode("Unsainted ", "SLIPKNOT", "METAL", 7);
	yey.AddNode("A Lier's Funeral ", "SLIPKNOT", "METAL", 7);
	yey.AddNode("Solway Firth ", "SLIPKNOT", "METAL", 7);
	yey.AddNode("Birth Of the Cruel ", "SLIPKNOT", "METAL", 7);
	yey.AddNode("Orphan ", "SLIPKNOT", "METAL", 7);
	yey.AddNode("Critical Darling ", "SLIPKNOT", "METAL", 7);
	yey.AddNode("My Pain ", "SLIPKNOT", "METAL", 7);
	yey.AddNode("Red Flag ", "SLIPKNOT", "METAL", 7);
	yey.AddNode("Not Long for this world ", "SLIPKNOT", "METAL", 7);
	yey.AddNode("Death because of death ", "SLIPKNOT", "METAL", 7);
	yey.AddNode("What's Next? ", "SLIPKNOT", "METAL", 7);
	//Enema of the State 12
	yey.AddNode("Dumpweed ", "Blink 182", "POP-PUNK", 9);
	yey.AddNode("Dont Leave Me ", "Blink 182", "POP-PUNK", 9);
	yey.AddNode("Aliens Exist ", "Blink 182", "POP-PUNK", 9);
	yey.AddNode("Going Away To College ", "Blink 182", "POP-PUNK", 9);
	yey.AddNode("What's My Age Again' ", "Blink 182", "POP-PUNK", 9);
	yey.AddNode("Dysentry Gary ", "Blink 182", "POP-PUNK", 9);
	yey.AddNode("Adam's Song ", "Blink 182", "POP-PUNK", 9);
	yey.AddNode("All The Small Things ", "Blink 182", "POP-PUNK", 9);
	yey.AddNode("The Party Song ", "Blink 182", "POP-PUNK", 9);
	yey.AddNode("Mutt ", "Blink 182", "POP-PUNK", 9);
	yey.AddNode("Wendy Clear ", "Blink 182", "POP-PUNK", 9);
	yey.AddNode("Anthem ", "Blink 182", "POP-PUNK", 9);
	//American FootBall 9
	yey.AddNode("Never Meant ", "American Football",  "EMO", 4);
	yey.AddNode("The Summer Ends ", "American Football", "EMO", 4);
	yey.AddNode("Honestly ", "American Football", "EMO", 4);
	yey.AddNode("For Sure ","American Football",  "EMO", 4);
	yey.AddNode("You Know I Should Be Leaving Soon ", "American Football", "EMO", 4);
	yey.AddNode("But The Regrets Are Killing Me ", "American Football", "EMO", 4);
	yey.AddNode("I'll See You When We're Both Not So Emotional ", "American Football", "EMO", 4);
	yey.AddNode("Stay Home ", "American Football", "EMO", 4);
	yey.AddNode("The One With The Wurlitzer ", "American Football", "EMO", 4);
	//LNOTGY 10
	yey.AddNode("Citezens Of Earth ", "Neck Deep", "POP-PUNK", 9);
	yey.AddNode("I Hope This Comes Back To Haunt You ", "Neck Deep", "POP-PUNK", 9);
	yey.AddNode("December ", "Neck Deep", "POP-PUNK", 9);
	yey.AddNode("Smooth Seas Don�t Make Good Sailors ", "Neck Deep", "POP-PUNK", 9);
	yey.AddNode("Cant Kick Up The Roots ", "Neck Deep", "POP-PUNK", 9);
	yey.AddNode("Gold Steps ", "Neck Deep", "POP-PUNK", 9);
	yey.AddNode("Kali Ma ", "Neck Deep", "POP-PUNK", 9);
	yey.AddNode("Rock Bottom ", "Neck Deep", "POP-PUNK", 9);
	yey.AddNode("The Beach is For Lovers Not Losers ", "Neck Deep", "POP-PUNK", 9);
	yey.AddNode("Serpents ", "Neck Deep", "POP-PUNK", 9);
	//Doo-Wops & Hooligans 11
	yey.AddNode("Grenade ", "Bruno Mars", "POP", 8);
	yey.AddNode("Just the Way you Are ", "Bruno Mars", "POP", 8);
	yey.AddNode("Our First Time ", "Bruno Mars", "POP", 8);
	yey.AddNode("Runaway Baby ", "Bruno Mars", "POP", 8);
	yey.AddNode("The Lazy Song ", "Bruno Mars", "POP", 8);
	yey.AddNode("Marry You ", "Bruno Mars", "POP", 8);
	yey.AddNode("Talking to the Moon ", "Bruno Mars", "POP", 8);
	yey.AddNode("Liquor Store Blues ", "Bruno Mars", "POP", 8);
	yey.AddNode("Count on Me ", "Bruno Mars", "POP", 8);
	yey.AddNode("The Other Side ", "Bruno Mars", "POP", 8);
	yey.AddNode("Somewhere in Brooklyn ", "Bruno Mars", "POP", 8);
	//Cutterpillow  13
	yey.AddNode("Superproxy ", "EraserHeads", "POP", 8);
	yey.AddNode("Back2me ", "EraserHeads", "POP", 8);
	yey.AddNode("Waiting for the Bus ", "EraserHeads", "POP", 8);
	yey.AddNode("Fine Time ", "EraserHeads", "POP", 8);
	yey.AddNode("Kama Supra ", "EraserHeads", "POP", 8);
	yey.AddNode("Overdrive ", "EraserHeads", "POP", 8);
	yey.AddNode("Slo Mo ", "EraserHeads", "POP", 8);
	yey.AddNode("Torpedo ", "EraserHeads", "POP", 8);
	yey.AddNode("Huwag mo nang Itanong ", "EraserHeads", "POP", 8);
	yey.AddNode("Paru-Parong Ningning ", "EraserHeads", "POP", 8);
	yey.AddNode("Walang Nagbago ", "EraserHeads", "POP", 8);
	yey.AddNode("Cutterpillow ", "EraserHeads", "POP", 8);
	yey.AddNode("Poorman's Grave ", "EraserHeads", "POP", 8);
	//  Save Rock & Roll
	yey.AddNode("The Phoenix ", "Fallout Boy", "POP-ROCK", 10);
	yey.AddNode("My Songs Know What You Did in the Dark (Light Em Up) ", "Fallout Boy", "POP-ROCK", 10);
	yey.AddNode("Alone Together ", "Fallout Boy", "POP-ROCK", 10);
	yey.AddNode("Where Did the Party Go ", "Fallout Boy", "POP-ROCK", 10);
	yey.AddNode("Just One Yesterday ", "Fallout Boy", "POP-ROCK", 10);
	yey.AddNode("The Mighty Fall ", "Fallout Boy", "POP-ROCK", 10);
	yey.AddNode("Miss Missing You ", "Fallout Boy", "POP-ROCK", 10);
	yey.AddNode("Death Valley ", "Fallout Boy", "POP-ROCK", 10);
	yey.AddNode("Young Volcanoes ", "Fallout Boy", "POP-ROCK", 10);
	yey.AddNode("Rat a Tat ", "Fallout Boy", "POP-ROCK", 10);
	yey.AddNode("Save Rock and Roll ", "Fallout Boy", "POP-ROCK", 10);
	// Clarity
	yey.AddNode("Hourglass ", "Zedd", "POP", 8);
	yey.AddNode("Shave it ", "Zedd", "POP", 8);
	yey.AddNode("Spectrum ", "Zedd", "POP", 8);
	yey.AddNode("Lost at Sea ", "Zedd", "POP", 8);
	yey.AddNode("Clarity ", "Zedd", "POP", 8);
	yey.AddNode("Codec ", "Zedd", "POP", 8);
	yey.AddNode("Stache ", "Zedd", "POP", 8);
	yey.AddNode("Fall in to Sky ", "Zedd", "POP", 8);
	yey.AddNode("Follow you Down ", "Zedd", "POP", 8);
	yey.AddNode("Epos ", "Zedd", "POP", 8);
	yey.AddNode("Stay The Night ", "Zedd", "POP", 8);
	yey.AddNode("Push Play ", "Zedd", "POP", 8);
	yey.AddNode("Alive ", "Zedd", "POP", 8);
	yey.AddNode("Breakin a Sweat ", "Zedd", "POP", 8);
	//NOthing Personal
	yey.AddNode("Weightless ", "All Time Low", "POP-PUNK", 9);
	yey.AddNode("Break Your Little Heart ", "All Time Low", "POP-PUNK", 9);
	yey.AddNode("Damned If I Do Ya, Damned If I Don't ", "All Time Low", "POP-PUNK", 9);
	yey.AddNode("Lost In Stereo ", "All Time Low", "POP-PUNK", 9);
	yey.AddNode("Stella ", "All Time Low", "POP-PUNK", 9);
	yey.AddNode("Sick Little Games ", "All Time Low", "POP-PUNK", 9);
	yey.AddNode("Hello, Brooklyn ", "All Time Low", "POP-PUNK", 9);
	yey.AddNode("Walls ", "All Time Low", "POP-PUNK", 9);
	yey.AddNode("Too Much ", "All Time Low", "POP-PUNK", 9);
	yey.AddNode("Keep The Change, You Filthy Animal ", "All Time Low", "POP-PUNK", 9);
	yey.AddNode("A Party Song (The Walk Of Shame) ", "All Time Low", "POP-PUNK", 9);
	yey.AddNode("Therapy ", "All Time Low", "POP-PUNK", 9);
	//All We Know is Falling
	yey.AddNode("Pressure ", "Paramore", "POP-ROCK", 10);
	yey.AddNode("All We Know ", "Paramore", "POP-ROCK", 10);
	yey.AddNode("Emergency ", "Paramore", "POP-ROCK", 10);
	yey.AddNode("Brighter ", "Paramore", "POP-ROCK", 10);
	yey.AddNode("Here We Go Again ", "Paramore", "POP-ROCK", 10);
	yey.AddNode("Let This Go ", "Paramore", "POP-ROCK", 10);
	yey.AddNode("Woah ", "Paramore", "POP-ROCK", 10);
	yey.AddNode("Conspiracy ", "Paramore", "POP-ROCK", 10);
	yey.AddNode("Franklin ", "Paramore", "POP-ROCK", 10);
	yey.AddNode("My Heart ", "Paramore", "POP-ROCK", 10);
	//If You Were a Movie, This Would Be Your Soundtrack
	yey.AddNode("Scene One - James Dean & Audrey Hepburn ", "Sleeping With Sirens", "ACOUSTIC", 1);
	yey.AddNode("Scene Two - Roger Rabbit ", "Sleeping With Sirens", "ACOUSTIC", 1);
	yey.AddNode("Scene Three - Stomach Tied In Knots ", "Sleeping With Sirens", "ACOUSTIC", 1);
	yey.AddNode("Scene Four - Don't You Ever Forget About Me ", "Sleeping With Sirens", "ACOUSTIC", 1);
	yey.AddNode("Scene Five - With Ears To See and Eyes To Hear ", "Sleeping With Sirens", "ACOUSTIC", 1);
	//The Finer Things (Acoustic)
	yey.AddNode("Elevated ", "State Champs", "ACOUSTIC", 1);
	yey.AddNode("Deadly Conversation ", "State Champs", "ACOUSTIC", 1);
	yey.AddNode("Hard To Please ", "State Champs", "ACOUSTIC", 1);
	yey.AddNode("Prepare to be Noticed ", "State Champs", "ACOUSTIC", 1);
	yey.AddNode("Over The Line ", "State Champs", "ACOUSTIC", 1);
	yey.AddNode("Sample Existence ", "State Champs", "ACOUSTIC", 1);
	yey.AddNode("Remedy ", "State Champs", "ACOUSTIC", 1);
	yey.AddNode("Nothing's Wrong ", "State Champs", "ACOUSTIC", 1);
	yey.AddNode("Mind Bottled ", "State Champs", "ACOUSTIC", 1);
	yey.AddNode("Critical ", "State Champs", "ACOUSTIC", 1);
	yey.AddNode("Easy Enough ", "State Champs", "ACOUSTIC", 1);
	//Science & Faith
	yey.AddNode("Nothing ", "The Script", "ALTERNATIVE", 2);
	yey.AddNode("If you ever come back ", "The Script", "ALTERNATIVE", 2);
	yey.AddNode("Science & Faith ", "The Script", "ALTERNATIVE", 2);
	yey.AddNode("Long Gone and Moved on ", "The Script", "ALTERNATIVE", 2);
	yey.AddNode("Deadman Walking ", "The Script", "ALTERNATIVE", 2);
	yey.AddNode("This Love ", "The Script", "ALTERNATIVE", 2);
	yey.AddNode("Walk Away ", "The Script", "ALTERNATIVE", 2);
	yey.AddNode("Exit Wounds ", "The Script", "ALTERNATIVE", 2);
	yey.AddNode("You won't feel a Thing ", "The Script", "ALTERNATIVE", 2);
	//Move Along
	yey.AddNode("Dirty Little Secret ", "The All-American Reject", "POP-PUNK", 9);
	yey.AddNode("Stab My Back ", "The All-American Reject", "POP-PUNK", 9);
	yey.AddNode("Move Along ", "The All-American Reject", "POP-PUNK", 9);
	yey.AddNode("It Ends Tonight ", "The All-American Reject", "POP-PUNK", 9);
	yey.AddNode("Change Your Mind ", "The All-American Reject", "POP-PUNK", 9);
	yey.AddNode("Night Drive ", "The All-American Reject", "POP-PUNK", 9);
	yey.AddNode("11:11 PM ", "The All-American Reject", "POP-PUNK", 9);
	yey.AddNode("Dance Inside ", "The All-American Reject", "POP-PUNK", 9);
	yey.AddNode("Top of the World ", "The All-American Reject", "POP-PUNK", 9);
	yey.AddNode("Straitjacket Feeling  ", "The All-American Reject", "POP-PUNK", 9);
	yey.AddNode("I'm waiting ", "The All-American Reject", "POP-PUNK", 9);
	yey.AddNode("Can't take it ", "The All-American Reject", "POP-PUNK", 9);
	//Sweetener
	yey.AddNode("God Is A Woman ", "Ariana Grande", "POP", 8);
	yey.AddNode("Sweetener ", "Ariana Grande", "POP", 8);
	yey.AddNode("Breathin ", "Ariana Grande", "POP", 8);
	yey.AddNode("No Tears Left To Cry ", "Ariana Grande", "POP", 8);
	yey.AddNode("Everytime ", "Ariana Grande", "POP", 8);
	yey.AddNode("Get Well Soon", "Ariana Grande", "POP", 8);
	yey.AddNode("Goodnight n Go ", "Ariana Grande", "POP", 8);
	yey.AddNode("R.E.M. ", "Ariana Grande", "POP", 8);
	yey.AddNode("The Light is Coming ", "Ariana Grande", "POP", 8);
	//Red
	yey.AddNode("State of Grace ", "Taylor Swift", "POP", 8);
	yey.AddNode("Red ", "Taylor Swift", "POP", 8);
	yey.AddNode("I Knew You Were Trouble ", "Taylor Swift", "POP", 8);
	yey.AddNode("All Too Well ", "Taylor Swift", "POP", 8);
	yey.AddNode("22", "Taylor Swift", "POP", 8);
	yey.AddNode("I Almost Do ", "Taylor Swift", "POP", 8);
	yey.AddNode("We Are Never Ever Getting Back Together ", "Taylor Swift", "POP", 8);
	yey.AddNode("Starlight ", "Taylor Swift", "POP", 8);
	yey.AddNode("Holy Ground ", "Taylor Swift", "POP", 8);
	yey.AddNode("Begin Again", "Taylor Swift", "POP", 8);
	//Hopeless Fountain Kingdom
	yey.AddNode("Eyes Closed ", "Hasley", "INDIE", 6);
	yey.AddNode("Alone ", "Hasley", "INDIE", 6);
	yey.AddNode("Now or Never ", "Hasley", "INDIE", 6);
	yey.AddNode("Sorry ", "Hasley", "INDIE", 6);
	yey.AddNode("Bad At Love ", "Hasley", "INDIE", 6);
	yey.AddNode("Devil In Me ", "Hasley", "INDIE", 6);
	//Honey Works
	yey.AddNode("Assertion of the Heart", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Bae Love", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Bloom in Love Color", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Brazen Honey", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Can I confess to you?", "HoneyWorks", "ROCK", 13);
	yey.AddNode("The day I knew Love	", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Declaration of the Weak", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Dream Fanfare ", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Fansa ", "HoneyWorks", "ROCK", 13);
	yey.AddNode("I like you now ", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Inokori-sensei  ", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Light Proof Theory ", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Love meets and love continues ", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Maidens	", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Mama ", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Monday's Melancholy ","HoneyWorks",  "ROCK", 13);
	yey.AddNode("Nostalgic Rainfall ", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Picture Book of my first love ", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Secret of Sunday ", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Sick name love wazurai", "HoneyWorks", "ROCK", 13);
	yey.AddNode("A small lion ", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Tokyo summer session ", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Twins", "HoneyWorks", "ROCK", 13);
	//Mind Of Mine
	yey.AddNode("PILLOWTALK ", "ZAYN", "POP", 8);
	yey.AddNode("?iT�s YoU ", "ZAYN", "POP", 8);
	yey.AddNode("BeFoUr ", "ZAYN", "POP", 8);
	yey.AddNode("sHe ", "ZAYN", "POP", 8);
	yey.AddNode("dRuNk ", "ZAYN", "POP", 8);
	yey.AddNode("?rEaR vIeW ", "ZAYN", "POP", 8);
	yey.AddNode("?wRoNg ", "ZAYN", "POP", 8);
	yey.AddNode("?fOoL fOr YoU ", "ZAYN", "POP", 8);
	yey.AddNode("BoRdErZ ", "ZAYN", "POP", 8);
	yey.AddNode("?tRuTh ", "ZAYN", "POP", 8);
	yey.AddNode("lUcOzAdE ", "ZAYN", "POP", 8);
	yey.AddNode("TiO ", "ZAYN", "POP", 8);
	yey.AddNode("BLUE ", "ZAYN", "POP", 8);
	yey.AddNode("BRIGHT ", "ZAYN", "POP", 8);
	yey.AddNode("LIKE I WOULD ", "ZAYN", "POP", 8);
	yey.AddNode("SHE DON�T LOVE ME ", "ZAYN", "POP", 8);
	//FOUR
	yey.AddNode("Steal My Girl ", "One Direction", "POP", 8);
	yey.AddNode("Ready to Run ", "One Direction", "POP", 8);
	yey.AddNode("Where Do Broken Hearts Go ", "One Direction", "POP", 8);
	yey.AddNode("18 ", "One Direction", "POP", 8);
	yey.AddNode("Girl Almighty ", "One Direction", "POP", 8);
	yey.AddNode("Fool�s Gold ", "One Direction", "POP", 8);
	yey.AddNode("Night Changes ", "One Direction", "POP", 8);
	yey.AddNode("No Control ", "One Direction", "POP", 8);
	yey.AddNode("Fireproof ", "One Direction", "POP", 8);
	yey.AddNode("Spaces ", "One Direction", "POP", 8);
	yey.AddNode("Stockholm Syndrome ", "One Direction", "POP", 8);
	yey.AddNode("Clouds ", "One Direction", "POP", 8);
	yey.AddNode("Change Your Ticket ", "One Direction", "POP", 8);
	yey.AddNode("Illusion ", "One Direction", "POP", 8);
	yey.AddNode("Once in a Lifetime ", "One Direction", "POP", 8);
	yey.AddNode("Act My Age ", "One Direction", "POP", 8);
	//Multiply
	yey.AddNode("One ", "Ed Sheeran", "POP", 8);
	yey.AddNode("I'm a mess ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Sing ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Don't ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Nina ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Photograph ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Bloodstream ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Terenife Sea ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Thinking out loud ", "Ed Sheeran", "POP", 8);
	//Divide
	yey.AddNode("Eraser ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Castle on the hill ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Dive ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Shape of you ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Perfect ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Galway Girl ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Happier ", "Ed Sheeran", "POP", 8);
	yey.AddNode("New man ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Hearts don't break around here ", "Ed Sheeran", "POP", 8);
	yey.AddNode("What do i know ", "Ed Sheeran", "POP", 8);
	yey.AddNode("How do you feel ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Supermarket Flowers ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Barcelona ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Bibia be ye ye ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Nancy Mulligan ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Save yourself ", "Ed Sheeran", "POP", 8);
	//1989
	yey.AddNode("Welcome To New York ", "Taylor Swift", "POP", 8);
	yey.AddNode("Blank Space ", "Taylor Swift", "POP", 8);
	yey.AddNode("Style ", "Taylor Swift", "POP", 8);
	yey.AddNode("Out Of The Woods ", "Taylor Swift", "POP", 8);
	yey.AddNode("All you had to do was stay ", "Taylor Swift", "POP", 8);
	yey.AddNode("Shake it Off ", "Taylor Swift", "POP", 8);
	yey.AddNode("I wish you would ", "Taylor Swift", "POP", 8);
	yey.AddNode("Bad Blood ", "Taylor Swift", "POP", 8);
	yey.AddNode("Wildest Dreams ", "Taylor Swift", "POP", 8);
	yey.AddNode("How You Get The Girl ", "Taylor Swift", "POP", 8);
	yey.AddNode("This Love ", "Taylor Swift", "POP", 8);
	yey.AddNode("I know Places ", "Taylor Swift", "POP", 8);
	yey.AddNode("Clean ", "Taylor Swift", "POP", 8);
	//After Laughter
	yey.AddNode("Hard Times ", "Paramore", "ALTERNATIVE", 2);
	yey.AddNode("Rose-Colored Boy ", "Paramore", "ALTERNATIVE", 2);
	yey.AddNode("Told You So ", "Paramore", "ALTERNATIVE", 2);
	yey.AddNode("Forgiveness ", "Paramore", "ALTERNATIVE", 2);
	yey.AddNode("Fake Happy ", "Paramore", "ALTERNATIVE", 2);
	yey.AddNode("26 ", "Paramore", "ALTERNATIVE", 2);
	yey.AddNode("Pool ", "Paramore", "ALTERNATIVE", 2);
	yey.AddNode("Grudges ", "Paramore", "ALTERNATIVE", 2);
	yey.AddNode("Caught In the Middle ", "Paramore", "ALTERNATIVE", 2);
	yey.AddNode("Idle Worship ", "Paramore", "ALTERNATIVE", 2);
	yey.AddNode("No Friend ", "Paramore", "ALTERNATIVE", 2);
	yey.AddNode("Tell Me How ", "Paramore", "ALTERNATIVE", 2);
	//Need You Now
	yey.AddNode("Need You Now ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("Our Kind Of Love ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("American Honey ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("Hello World ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("Perfect Day ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("Love This Pain ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("When You Got A Good Thing ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("Stars Tonight ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("If I Know Then ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("Something Bout A Woman ", "Lady Antibellum", "COUNTRY", 3);
	//HeartBreak
	yey.AddNode("Home ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("Heart Break ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("Somebody's Else's Heart ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("This City ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("Hurt ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("Army ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("Good Time To Be Alive ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("Think About You ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("Big Love In A Small Town ", "Lady Antibellum", "COUNTRY", 3);
	//Symphony Soldier
	yey.AddNode("Angel With A Shotgun ", "The Cab", "ALTERNATIVE", 2);
	yey.AddNode("Endlessly  ", "The Cab", "ALTERNATIVE", 2);
	yey.AddNode("Animal ", "The Cab", "ALTERNATIVE", 2);
	yey.AddNode("Intoxicated ", "The Cab", "ALTERNATIVE", 2);
	yey.AddNode("Lala ", "The Cab", "ALTERNATIVE", 2);
	yey.AddNode("Her Love Is My Religion ", "The Cab", "ALTERNATIVE", 2);
	yey.AddNode("Another Me ", "The Cab", "ALTERNATIVE", 2);
	yey.AddNode("Lovesick Fool ", "The Cab", "ALTERNATIVE", 2);
	//Freudian
	yey.AddNode("Get You ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("Best Part ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("Hold Me DOwn ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("Nue Roses ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("Loose ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("We Find Love ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("Blessed ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("Take Me Away ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("Transform ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("Freudian ", "Daniel Caesar", "R&B", 12);
	//Case Study 01
	yey.AddNode("LOVE AGAIN ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("SUPER POSITION ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("TOO DEEP TO TURN BACK ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("FRONTAL LOBE MUZIK ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("CYANIDE ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("ARE YOU OKAY? ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("OPEN UP ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("ENTROPY ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("RESTORE THE FEELING ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("COMPLEXITIES ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("loveAgain ", "Daniel Caesar", "R&B", 12);
	// Natty Dread
	yey.AddNode("Lively Up Yourself ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("No Woman No Cry ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Them Belly Full ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Rebel Music ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("So Jah Seh ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Natty Dread ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Bend Down Low ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Talkin Blues ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Ravolution ", "Bob Marley and the Wailers", "REGGAE", 11);
	//Legend
	yey.AddNode("Is This Love ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("No Woman No Cry ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Could You Be Loved ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Three Little Birds ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Buffalo Soldier ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Get Up Stand Up", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Stir It Up ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("One Love ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("I Shot The Sheriff ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Waiting In Vain ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Redemption Song ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Satisfy My Soul ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Exodus ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Jamming ", "Bob Marley and the Wailers", "REGGAE", 11);
	//Rastaman Vibrations
	yey.AddNode("Positive Vibrations ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Roots, Rocks, Reggae ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Johnny Was ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Cry To Me ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Want More ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Crazy Bald Head ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Who The Cap Fit", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Night Shift ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("War ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Rat Race ", "Bob Marley and the Wailers", "REGGAE", 11);
	//4 your Eyez only
	yey.AddNode("4 Your Eyez Only ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("Change ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("Deja Vu ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("Foldin Clothes ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("For Whom The Bells Toll ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("Immortal ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("Neighbors ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("She's Mine Pt. 5 ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("She's Mine Pt. 2 ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("Ville Mentality ", "JCOLE", "HIPHOP", 5);
	//Russ The Mixtape
	yey.AddNode("For the Stunt ", "Russ", "HIPHOP", 5);
	yey.AddNode("Aint NobodyTakin My Baby ", "Russ", "HIPHOP", 5);
	yey.AddNode("2 A.M. ", "Russ", "HIPHOP", 5);
	yey.AddNode("Down For You ", "Russ", "HIPHOP", 5);
	yey.AddNode("Off The Strength ", "Russ", "HIPHOP", 5);
	yey.AddNode("We Just Havent Met Yet ", "Russ", "HIPHOP", 5);
	yey.AddNode("Always Knew", "Russ", "HIPHOP", 5);
	yey.AddNode("Whenever", "Russ", "HIPHOP", 5);
	yey.AddNode("Waste My Time", "Russ", "HIPHOP", 5);
	yey.AddNode("Inbetween", "Russ", "HIPHOP", 5);
	yey.AddNode("Inbetween", "Russ", "HIPHOP", 5);
	yey.AddNode("Lost", "Russ", "HIPHOP", 5);
	yey.AddNode("Yung God", "Russ", "HIPHOP", 5);
	yey.AddNode("Lately", "Russ", "HIPHOP", 5);
	yey.AddNode("Comin' Thru", "Russ", "HIPHOP", 5);
	yey.AddNode("Connected", "Russ", "HIPHOP", 5);
	yey.AddNode("Keep the Faith", "Russ", "HIPHOP", 5);
	yey.AddNode("All My Angels", "Russ", "HIPHOP", 5);
	yey.AddNode("Brush Me (feat. Colliee Buddz)", "Russ", "HIPHOP", 5);
	//Kupal Destroyerr 1
	yey.AddNode("Lamasin Ang Osus", "TUBERO", "METAL", 7);
	yey.AddNode("Animal ", "TUBERO", "METAL", 7);
	yey.AddNode("Coming ", "TUBERO", "METAL", 7);
	yey.AddNode("Makapal ang Blbl mo ", "TUBERO", "METAL", 7);
	yey.AddNode("Sir tngna mo pkyu ALbum VEr ", "TUBERO", "METAL", 7);
	yey.AddNode("Isubo ang ulo ", "TUBERO", "METAL", 7);
	yey.AddNode("Ptngna 2 ", "TUBERO", "METAL", 7);
	yey.AddNode("PtngnaTrapik ka ", "TUBERO", "METAL", 7);
	yey.AddNode("Sira Ulo ka ", "TUBERO", "METAL", 7);
	yey.AddNode("Ptngna 1 ", "TUBERO", "METAL", 7);
	yey.AddNode("Anak ka ng Pakyu Album Ver ", "TUBERO", "METAL", 7);
	yey.AddNode("Kupal Ka ", "TUBERO", "METAL", 7);
	yey.AddNode("Galit ako sa Gwapo ", "TUBERO", "METAL", 7);
	yey.AddNode("Mamatay ka na ", "TUBERO", "METAL", 7);
	yey.AddNode("Mamatay ka na sana ", "TUBERO", "METAL", 7);
	yey.AddNode("Gard Tngna mo ", "TUBERO", "METAL", 7);
	yey.AddNode("BAstos Ka ", "TUBERO", "METAL", 7);	
	yey.AddNode("Mamatay ka na sana Album VEr", "TUBERO", "METAL", 7);
	yey.AddNode("Sir Tanginamo Pakyuuu ", "TUBERO", "METAL", 7);
	//December Avenue (20110)
	yey.AddNode("City Lights ", "December Avenue", "ALTERNATIVE", 2);
	yey.AddNode("Sleep Tonight ", "December Avenue", "ALTERNATIVE", 2);
	yey.AddNode("Fallin� ", "December Avenue", "ALTERNATIVE", 2);
	yey.AddNode("Ears and Rhymes ", "December Avenue", "ALTERNATIVE", 2);
	yey.AddNode("I�ll Be Watching You ", "December Avenue", "ALTERNATIVE", 2);
	yey.AddNode("Back To Love ", "December Avenue", "ALTERNATIVE", 2);
	yey.AddNode("Eroplanong Papel ", "December Avenue", "ALTERNATIVE", 2);
	yey.AddNode("Dive ", "December Avenue", "ALTERNATIVE", 2);
	yey.AddNode("Forever ", "December Avenue", "ALTERNATIVE", 2);
	yey.AddNode("Breathe Again ", "December Avenue", "ALTERNATIVE", 2);
	yey.AddNode("Time To Go ", "December Avenue", "ALTERNATIVE", 2);
	//Too Weird To Live, Too Rare To Die!
	yey.AddNode("Casual Affair ", "Panic! At The Disco", "POP-ROCK", 10);
	yey.AddNode("Collar Full  ", "Panic! At The Disco", "POP-ROCK", 10);
	yey.AddNode("Girl That You Love ", "Panic! At The Disco", "POP-ROCK", 10);
	yey.AddNode("Miss Jackson ", "Panic! At The Disco", "POP-ROCK", 10);
	yey.AddNode("Nicotine ", "Panic! At The Disco", "POP-ROCK", 10);
	yey.AddNode("The End Of All Things ", "Panic! At The Disco", "POP-ROCK", 10);
	yey.AddNode("This Is Gospel ", "Panic! At The Disco", "POP-ROCK", 10);
	yey.AddNode("Vegas Lights ", "Panic! At The Disco", "POP-ROCK", 10);
	//Master Of Puppets
	yey.AddNode("Battery ", "METALLICA", "METAL", 7);
	yey.AddNode("Damage, Inc. ", "METALLICA", "METAL", 7);
	yey.AddNode("Disposable Heroes ", "METALLICA", "METAL", 7);
	yey.AddNode("Leper Messiah ", "METALLICA", "METAL", 7);
	yey.AddNode("Master Of Puppets ", "METALLICA", "METAL", 7);
	yey.AddNode("Orion ", "METALLICA", "METAL", 7);
	yey.AddNode("The Thing That Should Be ", "METALLICA", "METAL", 7);
	yey.AddNode("Welcome Home (Sanitarium) ", "METALLICA", "METAL", 7);
	//Get Your Heart On!
	yey.AddNode("You Suck at Love ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Can't Keep My Hands off You ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Jet Lag ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Astronaut ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Loser of the Year ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Anywhere Else But Here ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Freaking Me Out ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Summer Paradise ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Gone Too Soon ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Last One Standing ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("This Song Saved My Life ", "Simple Plan", "POP-PUNK", 9);
	//Still Not Getting Any
	yey.AddNode("Shut Up! ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Welcome to My Life ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Perfect World ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Thank You ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Me Against the World ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Jump ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Everytime ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Promise ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("One ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Untitled ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Perfect ", "Simple Plan", "POP-PUNK", 9);
	//Vessel
	yey.AddNode("Ode To sleep ", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("Holding on to you ", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("Migraine ", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("House Of gold ", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("car Radio ", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("Semi Automatic ", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("SCreen ", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("The Run and Go ", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("Fake You Out ", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("Guns for Hands ", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("Trees ", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("Truce ", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("Holding on to you ", "Twenty One Pilots", "EMO", 4);
	//Blurry Face
	yey.AddNode("Heavy Dirty Soul - Twenty One Pilots", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("Stressed Out - Twenty One Pilots", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("Ride - Twenty One Pilots", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("Fairly Local - Twenty One Pilots", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("Tear in my heart - Twenty One Pilots", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("Lane Boy - Twenty One Pilots", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("The Judge - Twenty One Pilots", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("Doubt - Twenty One Pilots", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("Polarize - Twenty One Pilots", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("We Dont Believe What's on TV - Twenty One Pilots", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("Message Man - Twenty One Pilots", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("Home Town - Twenty One Pilots", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("Not Today - Twenty One Pilots", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("Goner - Twenty One Pilots", "Twenty One Pilots", "ROCK", 4);

	yey.AddNode("Dizzy On The Comedown ","Turnover","ALTERNATIVE",2);
	yey.AddNode("Cutting My Fingers Off ","Turnover","ALTERNATIVE",2);
	yey.AddNode("Take My Head ","Turnover","ALTERNATIVE",2);
	yey.AddNode("Diazepam ","Turnover","ALTERNATIVE",2);
	yey.AddNode("New Scream ","Turnover","ALTERNATIVE",2);
	yey.AddNode("Threshold ","Turnover","ALTERNATIVE",2);
	yey.AddNode("Humming ","Turnover","ALTERNATIVE",2);
	yey.AddNode("Like Slowly Disappearing ","Turnover","ALTERNATIVE",2);
	yey.AddNode("I Would Hate You If I Could ","Turnover","ALTERNATIVE",2);
	yey.AddNode("Intrapersonal ","Turnover","ALTERNATIVE",2);
	//FOREST HILLS DRIVE 2014 13
	yey.AddNode("Intro ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("January 28th ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("Wet Dreamz ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("03' Adolescence' ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("A tale of two Citiez ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("FireSquad ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("St Tropez ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("Hello ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("G.O.M.D ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("No Role Modelz ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("Apparently ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("Love Yourz ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("Note To Self ", "JCOLE", "HIPHOP", 5);
	// WE ARE NOT YOUR KIND 14
	yey.AddNode("Nero Forte ", "SLIPKNOT", "METAL", 7);
	yey.AddNode("Spiders ", "SLIPKNOT", "METAL", 7);	
	yey.AddNode("Insert Coin ", "SLIPKNOT", "METAL", 7);
	yey.AddNode("Unsainted ", "SLIPKNOT", "METAL", 7);
	yey.AddNode("A Lier's Funeral ", "SLIPKNOT", "METAL", 7);
	yey.AddNode("Solway Firth ", "SLIPKNOT", "METAL", 7);
	yey.AddNode("Birth Of the Cruel ", "SLIPKNOT", "METAL", 7);
	yey.AddNode("Orphan ", "SLIPKNOT", "METAL", 7);
	yey.AddNode("Critical Darling ", "SLIPKNOT", "METAL", 7);
	yey.AddNode("My Pain ", "SLIPKNOT", "METAL", 7);
	yey.AddNode("Red Flag ", "SLIPKNOT", "METAL", 7);
	yey.AddNode("Not Long for this world ", "SLIPKNOT", "METAL", 7);
	yey.AddNode("Death because of death ", "SLIPKNOT", "METAL", 7);
	yey.AddNode("What's Next? ", "SLIPKNOT", "METAL", 7);
	//Enema of the State 12
	yey.AddNode("Dumpweed ", "Blink 182", "POP-PUNK", 9);
	yey.AddNode("Dont Leave Me ", "Blink 182", "POP-PUNK", 9);
	yey.AddNode("Aliens Exist ", "Blink 182", "POP-PUNK", 9);
	yey.AddNode("Going Away To College ", "Blink 182", "POP-PUNK", 9);
	yey.AddNode("What's My Age Again' ", "Blink 182", "POP-PUNK", 9);
	yey.AddNode("Dysentry Gary ", "Blink 182", "POP-PUNK", 9);
	yey.AddNode("Adam's Song ", "Blink 182", "POP-PUNK", 9);
	yey.AddNode("All The Small Things ", "Blink 182", "POP-PUNK", 9);
	yey.AddNode("The Party Song ", "Blink 182", "POP-PUNK", 9);
	yey.AddNode("Mutt ", "Blink 182", "POP-PUNK", 9);
	yey.AddNode("Wendy Clear ", "Blink 182", "POP-PUNK", 9);
	yey.AddNode("Anthem ", "Blink 182", "POP-PUNK", 9);
	//American FootBall 9
	yey.AddNode("Never Meant ", "American Football", "EMO", 4);
	yey.AddNode("The Summer Ends ", "American Football", "EMO", 4);
	yey.AddNode("Honestly ", "American Football", "EMO", 4);
	yey.AddNode("For Sure ", "American Football", "EMO", 4);
	yey.AddNode("You Know I Should Be Leaving Soon ", "American Football", "EMO", 4);
	yey.AddNode("But The Regrets Are Killing Me ", "American Football", "EMO", 4);
	yey.AddNode("I'll See You When We're Both Not So Emotional ", "American Football", "EMO", 4);
	yey.AddNode("Stay Home ", "American Football", "EMO", 4);
	yey.AddNode("The One With The Wurlitzer ", "American Football", "EMO", 4);
	//LNOTGY 10
	yey.AddNode("Citezens Of Earth ", "Neck Deep", "POP-PUNK", 9);
	yey.AddNode("I Hope This Comes Back To Haunt You ", "Neck Deep", "POP-PUNK", 9);
	yey.AddNode("December ", "Neck Deep", "POP-PUNK", 9);
	yey.AddNode("Smooth Seas Don�t Make Good Sailors ", "Neck Deep", "POP-PUNK", 9);
	yey.AddNode("Cant Kick Up The Roots ", "Neck Deep", "POP-PUNK", 9);
	yey.AddNode("Gold Steps ", "Neck Deep", "POP-PUNK", 9);
	yey.AddNode("Kali Ma ", "Neck Deep", "POP-PUNK", 9);
	yey.AddNode("Rock Bottom ", "Neck Deep", "POP-PUNK", 9);
	yey.AddNode("The Beach is For Lovers Not Losers ", "Neck Deep", "POP-PUNK", 9);
	yey.AddNode("Serpents ", "Neck Deep", "POP-PUNK", 9);
	//Doo-Wops & Hooligans 11
	yey.AddNode("Grenade ", "Bruno Mars", "POP", 8);
	yey.AddNode("Just the Way you Are ", "Bruno Mars", "POP", 8);
	yey.AddNode("Our First Time ", "Bruno Mars", "POP", 8);
	yey.AddNode("Runaway Baby ", "Bruno Mars", "POP", 8);
	yey.AddNode("The Lazy Song ", "Bruno Mars", "POP", 8);
	yey.AddNode("Marry You ", "Bruno Mars", "POP", 8);
	yey.AddNode("Talking to the Moon ", "Bruno Mars", "POP", 8);
	yey.AddNode("Liquor Store Blues ", "Bruno Mars", "POP", 8);
	yey.AddNode("Count on Me ", "Bruno Mars", "POP", 8);
	yey.AddNode("The Other Side ", "Bruno Mars", "POP", 8);
	yey.AddNode("Somewhere in Brooklyn ", "Bruno Mars", "POP", 8);
	//Cutterpillow  13
	yey.AddNode("Superproxy ", "EraserHeads", "POP", 8);
	yey.AddNode("Back2me ", "EraserHeads", "POP", 8);
	yey.AddNode("Waiting for the Bus ", "EraserHeads", "POP", 8);
	yey.AddNode("Fine Time ", "EraserHeads", "POP", 8);
	yey.AddNode("Kama Supra ", "EraserHeads", "POP", 8);
	yey.AddNode("Overdrive ", "EraserHeads", "POP", 8);
	yey.AddNode("Slo Mo ", "EraserHeads", "POP", 8);
	yey.AddNode("Torpedo ", "EraserHeads", "POP", 8);
	yey.AddNode("Huwag mo nang Itanong ", "EraserHeads", "POP", 8);
	yey.AddNode("Paru-Parong Ningning ", "EraserHeads", "POP", 8);
	yey.AddNode("Walang Nagbago ", "EraserHeads", "POP", 8);
	yey.AddNode("Cutterpillow ", "EraserHeads", "POP", 8);
	yey.AddNode("Poorman's Grave ", "EraserHeads", "POP", 8);
	//  Save Rock & Roll
	yey.AddNode("The Phoenix ", "Fallout Boy", "POP-ROCK", 10);
	yey.AddNode("My Songs Know What You Did in the Dark (Light Em Up) ", "Fallout Boy", "POP-ROCK", 10);
	yey.AddNode("Alone Together ", "Fallout Boy", "POP-ROCK", 10);
	yey.AddNode("Where Did the Party Go ", "Fallout Boy", "POP-ROCK", 10);
	yey.AddNode("Just One Yesterday ", "Fallout Boy", "POP-ROCK", 10);
	yey.AddNode("The Mighty Fall ", "Fallout Boy", "POP-ROCK", 10);
	yey.AddNode("Miss Missing You ", "Fallout Boy", "POP-ROCK", 10);
	yey.AddNode("Death Valley ", "Fallout Boy", "POP-ROCK", 10);
	yey.AddNode("Young Volcanoes ", "Fallout Boy", "POP-ROCK", 10);
	yey.AddNode("Rat a Tat ", "Fallout Boy", "POP-ROCK", 10);
	yey.AddNode("Save Rock and Roll ", "Fallout Boy", "POP-ROCK", 10);
	// Clarity
	yey.AddNode("Hourglass ", "Zedd", "POP", 8);
	yey.AddNode("Shave it ", "Zedd", "POP", 8);
	yey.AddNode("Spectrum ", "Zedd", "POP", 8);
	yey.AddNode("Lost at Sea ", "Zedd", "POP", 8);
	yey.AddNode("Clarity ", "Zedd", "POP", 8);
	yey.AddNode("Codec ", "Zedd", "POP", 8);
	yey.AddNode("Stache ", "Zedd", "POP", 8);
	yey.AddNode("Fall in to Sky ", "Zedd", "POP", 8);
	yey.AddNode("Follow you Down ", "Zedd", "POP", 8);
	yey.AddNode("Epos ", "Zedd", "POP", 8);
	yey.AddNode("Stay The Night ", "Zedd", "POP", 8);
	yey.AddNode("Push Play ", "Zedd", "POP", 8);
	yey.AddNode("Alive ", "Zedd", "POP", 8);
	yey.AddNode("Breakin a Sweat ", "Zedd", "POP", 8);
	//NOthing Personal
	yey.AddNode("Weightless ", "All Time Low", "POP-PUNK", 9);
	yey.AddNode("Break Your Little Heart ", "All Time Low", "POP-PUNK", 9);
	yey.AddNode("Damned If I Do Ya, Damned If I Don't ", "All Time Low", "POP-PUNK", 9);
	yey.AddNode("Lost In Stereo ", "All Time Low", "POP-PUNK", 9);
	yey.AddNode("Stella ", "All Time Low", "POP-PUNK", 9);
	yey.AddNode("Sick Little Games ", "All Time Low", "POP-PUNK", 9);
	yey.AddNode("Hello, Brooklyn ", "All Time Low", "POP-PUNK", 9);
	yey.AddNode("Walls ", "All Time Low", "POP-PUNK", 9);
	yey.AddNode("Too Much ", "All Time Low", "POP-PUNK", 9);
	yey.AddNode("Keep The Change, You Filthy Animal ", "All Time Low", "POP-PUNK", 9);
	yey.AddNode("A Party Song (The Walk Of Shame) ", "All Time Low", "POP-PUNK", 9);
	yey.AddNode("Therapy ", "All Time Low", "POP-PUNK", 9);
	//All We Know is Falling
	yey.AddNode("Pressure ", "Paramore", "POP-ROCK", 10);
	yey.AddNode("All We Know ", "Paramore", "POP-ROCK", 10);
	yey.AddNode("Emergency ", "Paramore", "POP-ROCK", 10);
	yey.AddNode("Brighter ", "Paramore", "POP-ROCK", 10);
	yey.AddNode("Here We Go Again ", "Paramore", "POP-ROCK", 10);
	yey.AddNode("Let This Go ", "Paramore", "POP-ROCK", 10);
	yey.AddNode("Woah ", "Paramore", "POP-ROCK", 10);
	yey.AddNode("Conspiracy ", "Paramore", "POP-ROCK", 10);
	yey.AddNode("Franklin ", "Paramore", "POP-ROCK", 10);
	yey.AddNode("My Heart ", "Paramore", "POP-ROCK", 10);
	//If You Were a Movie, This Would Be Your Soundtrack
	yey.AddNode("Scene One - James Dean & Audrey Hepburn ", "Sleeping With Sirens", "ACOUSTIC", 1);
	yey.AddNode("Scene Two - Roger Rabbit ", "Sleeping With Sirens", "ACOUSTIC", 1);
	yey.AddNode("Scene Three - Stomach Tied In Knots ", "Sleeping With Sirens", "ACOUSTIC", 1);
	yey.AddNode("Scene Four - Don't You Ever Forget About Me ", "Sleeping With Sirens", "ACOUSTIC", 1);
	yey.AddNode("Scene Five - With Ears To See and Eyes To Hear ", "Sleeping With Sirens", "ACOUSTIC", 1);
	//The Finer Things (Acoustic)
	yey.AddNode("Elevated ", "State Champs", "ACOUSTIC", 1);
	yey.AddNode("Deadly Conversation ", "State Champs", "ACOUSTIC", 1);
	yey.AddNode("Hard To Please ", "State Champs", "ACOUSTIC", 1);
	yey.AddNode("Prepare to be Noticed ", "State Champs", "ACOUSTIC", 1);
	yey.AddNode("Over The Line ", "State Champs", "ACOUSTIC", 1);
	yey.AddNode("Sample Existence ", "State Champs", "ACOUSTIC", 1);
	yey.AddNode("Remedy ", "State Champs", "ACOUSTIC", 1);
	yey.AddNode("Nothing's Wrong ", "State Champs", "ACOUSTIC", 1);
	yey.AddNode("Mind Bottled ", "State Champs", "ACOUSTIC", 1);
	yey.AddNode("Critical ", "State Champs", "ACOUSTIC", 1);
	yey.AddNode("Easy Enough ", "State Champs", "ACOUSTIC", 1);
	//Science & Faith
	yey.AddNode("Nothing ", "The Script", "ALTERNATIVE", 2);
	yey.AddNode("If you ever come back ", "The Script", "ALTERNATIVE", 2);
	yey.AddNode("Science & Faith ", "The Script", "ALTERNATIVE", 2);
	yey.AddNode("Long Gone and Moved on ", "The Script", "ALTERNATIVE", 2);
	yey.AddNode("Deadman Walking ", "The Script", "ALTERNATIVE", 2);
	yey.AddNode("This Love ", "The Script", "ALTERNATIVE", 2);
	yey.AddNode("Walk Away ", "The Script", "ALTERNATIVE", 2);
	yey.AddNode("Exit Wounds ", "The Script", "ALTERNATIVE", 2);
	yey.AddNode("You won't feel a Thing ", "The Script", "ALTERNATIVE", 2);
	//Move Along
	yey.AddNode("Dirty Little Secret ", "The All-American Reject", "POP-PUNK", 9);
	yey.AddNode("Stab My Back ", "The All-American Reject", "POP-PUNK", 9);
	yey.AddNode("Move Along ", "The All-American Reject", "POP-PUNK", 9);
	yey.AddNode("It Ends Tonight ", "The All-American Reject", "POP-PUNK", 9);
	yey.AddNode("Change Your Mind ", "The All-American Reject", "POP-PUNK", 9);
	yey.AddNode("Night Drive ", "The All-American Reject", "POP-PUNK", 9);
	yey.AddNode("11:11 PM ", "The All-American Reject", "POP-PUNK", 9);
	yey.AddNode("Dance Inside ", "The All-American Reject", "POP-PUNK", 9);
	yey.AddNode("Top of the World ", "The All-American Reject", "POP-PUNK", 9);
	yey.AddNode("Straitjacket Feeling  ", "The All-American Reject", "POP-PUNK", 9);
	yey.AddNode("I'm waiting ", "The All-American Reject", "POP-PUNK", 9);
	yey.AddNode("Can't take it ", "The All-American Reject", "POP-PUNK", 9);
	//Sweetener
	yey.AddNode("God Is A Woman ", "Ariana Grande", "POP", 8);
	yey.AddNode("Sweetener ", "Ariana Grande", "POP", 8);
	yey.AddNode("Breathin ", "Ariana Grande", "POP", 8);
	yey.AddNode("No Tears Left To Cry ", "Ariana Grande", "POP", 8);
	yey.AddNode("Everytime ", "Ariana Grande", "POP", 8);
	yey.AddNode("Get Well Soon", "Ariana Grande", "POP", 8);
	yey.AddNode("Goodnight n Go ", "Ariana Grande", "POP", 8);
	yey.AddNode("R.E.M. ", "Ariana Grande", "POP", 8);
	yey.AddNode("The Light is Coming ", "Ariana Grande", "POP", 8);
	//Red
	yey.AddNode("State of Grace ", "Taylor Swift", "POP", 8);
	yey.AddNode("Red ", "Taylor Swift", "POP", 8);
	yey.AddNode("I Knew You Were Trouble ", "Taylor Swift", "POP", 8);
	yey.AddNode("All Too Well ", "Taylor Swift", "POP", 8);
	yey.AddNode("22", "Taylor Swift", "POP", 8);
	yey.AddNode("I Almost Do ", "Taylor Swift", "POP", 8);
	yey.AddNode("We Are Never Ever Getting Back Together ", "Taylor Swift", "POP", 8);
	yey.AddNode("Starlight ", "Taylor Swift", "POP", 8);
	yey.AddNode("Holy Ground ", "Taylor Swift", "POP", 8);
	yey.AddNode("Begin Again", "Taylor Swift", "POP", 8);
	//Hopeless Fountain Kingdom
	yey.AddNode("Eyes Closed ", "Hasley", "INDIE", 6);
	yey.AddNode("Alone ", "Hasley", "INDIE", 6);
	yey.AddNode("Now or Never ", "Hasley", "INDIE", 6);
	yey.AddNode("Sorry ", "Hasley", "INDIE", 6);
	yey.AddNode("Bad At Love ", "Hasley", "INDIE", 6);
	yey.AddNode("Devil In Me ", "Hasley", "INDIE", 6);
	//Honey Works
	yey.AddNode("Assertion of the Heart  ", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Bae Love  ", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Bloom in Love Color", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Brazen Honey ", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Can I confess to you?", "HoneyWorks", "ROCK", 13);
	yey.AddNode("The day I knew Love", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Declaration of the Weak", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Dream Fanfare", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Fansa ", "HoneyWorks", "ROCK", 13);
	yey.AddNode("I like you now ", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Inokori-sensei ", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Light Proof Theory ", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Love meets and love continues ", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Maidens", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Mama", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Monday's Melancholy ", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Nostalgic Rainfall", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Picture Book of my first love ", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Secret of Sunday ", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Sick name love wazurai ", "HoneyWorks", "ROCK", 13);
	yey.AddNode("A small lion ", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Tokyo summer session ", "HoneyWorks", "ROCK", 13);
	yey.AddNode("Twins", "HoneyWorks", "ROCK", 13);
	//Mind Of Mine
	yey.AddNode("PILLOWTALK ", "ZAYN", "POP", 8);
	yey.AddNode("?iT�s YoU ", "ZAYN", "POP", 8);
	yey.AddNode("BeFoUr ", "ZAYN", "POP", 8);
	yey.AddNode("sHe ", "ZAYN", "POP", 8);
	yey.AddNode("dRuNk ", "ZAYN", "POP", 8);
	yey.AddNode("?rEaR vIeW ", "ZAYN", "POP", 8);
	yey.AddNode("?wRoNg ", "ZAYN", "POP", 8);
	yey.AddNode("?fOoL fOr YoU ", "ZAYN", "POP", 8);
	yey.AddNode("BoRdErZ ", "ZAYN", "POP", 8);
	yey.AddNode("?tRuTh ", "ZAYN", "POP", 8);
	yey.AddNode("lUcOzAdE ", "ZAYN", "POP", 8);
	yey.AddNode("TiO ", "ZAYN", "POP", 8);
	yey.AddNode("BLUE ", "ZAYN", "POP", 8);
	yey.AddNode("BRIGHT ", "ZAYN", "POP", 8);
	yey.AddNode("LIKE I WOULD ", "ZAYN", "POP", 8);
	yey.AddNode("SHE DON�T LOVE ME ", "ZAYN", "POP", 8);
	//FOUR
	yey.AddNode("Steal My Girl ", "One Direction", "POP", 8);
	yey.AddNode("Ready to Run ", "One Direction", "POP", 8);
	yey.AddNode("Where Do Broken Hearts Go ", "One Direction", "POP", 8);
	yey.AddNode("18 ", "One Direction", "POP", 8);
	yey.AddNode("Girl Almighty ", "One Direction", "POP", 8);
	yey.AddNode("Fool�s Gold ", "One Direction", "POP", 8);
	yey.AddNode("Night Changes ", "One Direction", "POP", 8);
	yey.AddNode("No Control ", "One Direction", "POP", 8);
	yey.AddNode("Fireproof ", "One Direction", "POP", 8);
	yey.AddNode("Spaces ", "One Direction", "POP", 8);
	yey.AddNode("Stockholm Syndrome ", "One Direction", "POP", 8);
	yey.AddNode("Clouds ", "One Direction", "POP", 8);
	yey.AddNode("Change Your Ticket ", "One Direction", "POP", 8);
	yey.AddNode("Illusion ", "One Direction", "POP", 8);
	yey.AddNode("Once in a Lifetime ", "One Direction", "POP", 8);
	yey.AddNode("Act My Age ", "One Direction", "POP", 8);
	//Multiply
	yey.AddNode("One ", "Ed Sheeran", "POP", 8);
	yey.AddNode("I'm a mess ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Sing ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Don't ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Nina ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Photograph ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Bloodstream ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Terenife Sea ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Thinking out loud ", "Ed Sheeran", "POP", 8);
	//Divide
	yey.AddNode("Eraser ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Castle on the hill ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Dive ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Shape of you ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Perfect ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Galway Girl ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Happier ", "Ed Sheeran", "POP", 8);
	yey.AddNode("New man ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Hearts don't break around here ", "Ed Sheeran", "POP", 8);
	yey.AddNode("What do i know ", "Ed Sheeran", "POP", 8);
	yey.AddNode("How do you feel ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Supermarket Flowers ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Barcelona ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Bibia be ye ye ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Nancy Mulligan ", "Ed Sheeran", "POP", 8);
	yey.AddNode("Save yourself ", "Ed Sheeran", "POP", 8);
	//1989
	yey.AddNode("Welcome To New York ", "Taylor Swift", "POP", 8);
	yey.AddNode("Blank Space ", "Taylor Swift", "POP", 8);
	yey.AddNode("Style ", "Taylor Swift", "POP", 8);
	yey.AddNode("Out Of The Woods ", "Taylor Swift", "POP", 8);
	yey.AddNode("All you had to do was stay ", "Taylor Swift", "POP", 8);
	yey.AddNode("Shake it Off ", "Taylor Swift", "POP", 8);
	yey.AddNode("I wish you would ", "Taylor Swift", "POP", 8);
	yey.AddNode("Bad Blood ", "Taylor Swift", "POP", 8);
	yey.AddNode("Wildest Dreams ", "Taylor Swift", "POP", 8);
	yey.AddNode("How You Get The Girl ", "Taylor Swift", "POP", 8);
	yey.AddNode("This Love ", "Taylor Swift", "POP", 8);
	yey.AddNode("I know Places ", "Taylor Swift", "POP", 8);
	yey.AddNode("Clean ", "Taylor Swift", "POP", 8);
	//After Laughter
	yey.AddNode("Hard Times ", "Paramore", "ALTERNATIVE", 2);
	yey.AddNode("Rose-Colored Boy ", "Paramore", "ALTERNATIVE", 2);
	yey.AddNode("Told You So ", "Paramore", "ALTERNATIVE", 2);
	yey.AddNode("Forgiveness ", "Paramore", "ALTERNATIVE", 2);
	yey.AddNode("Fake Happy ", "Paramore", "ALTERNATIVE", 2);
	yey.AddNode("26 ", "Paramore", "ALTERNATIVE", 2);
	yey.AddNode("Pool ", "Paramore", "ALTERNATIVE", 2);
	yey.AddNode("Grudges ", "Paramore", "ALTERNATIVE", 2);
	yey.AddNode("Caught In the Middle ", "Paramore", "ALTERNATIVE", 2);
	yey.AddNode("Idle Worship ", "Paramore", "ALTERNATIVE", 2);
	yey.AddNode("No Friend ", "Paramore", "ALTERNATIVE", 2);
	yey.AddNode("Tell Me How ", "Paramore", "ALTERNATIVE", 2);
	//Need You Now
	yey.AddNode("Need You Now ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("Our Kind Of Love ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("American Honey ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("Hello World ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("Perfect Day ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("Love This Pain ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("When You Got A Good Thing ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("Stars Tonight ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("If I Know Then ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("Something Bout A Woman ", "Lady Antibellum", "COUNTRY", 3);
	//HeartBreak
	yey.AddNode("Home ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("Heart Break ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("Somebody's Else's Heart ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("This City ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("Hurt ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("Army ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("Good Time To Be Alive ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("Think About You ", "Lady Antibellum", "COUNTRY", 3);
	yey.AddNode("Big Love In A Small Town ", "Lady Antibellum", "COUNTRY", 3);
	//Symphony Soldier
	yey.AddNode("Angel With A Shotgun ", "The Cab", "ALTERNATIVE", 2);
	yey.AddNode("Endlessly  ", "The Cab", "ALTERNATIVE", 2);
	yey.AddNode("Animal ", "The Cab", "ALTERNATIVE", 2);
	yey.AddNode("Intoxicated ", "The Cab", "ALTERNATIVE", 2);
	yey.AddNode("Lala ", "The Cab", "ALTERNATIVE", 2);
	yey.AddNode("Her Love Is My Religion ", "The Cab", "ALTERNATIVE", 2);
	yey.AddNode("Another Me ", "The Cab", "ALTERNATIVE", 2);
	yey.AddNode("Lovesick Fool ", "The Cab", "ALTERNATIVE", 2);
	//Freudian
	yey.AddNode("Get You ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("Best Part ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("Hold Me DOwn ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("Nue Roses ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("Loose ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("We Find Love ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("Blessed ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("Take Me Away ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("Transform ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("Freudian ", "Daniel Caesar", "R&B", 12);
	//Case Study 01
	yey.AddNode("LOVE AGAIN ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("SUPER POSITION ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("TOO DEEP TO TURN BACK ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("FRONTAL LOBE MUZIK ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("CYANIDE ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("ARE YOU OKAY? ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("OPEN UP ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("ENTROPY ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("RESTORE THE FEELING ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("COMPLEXITIES ", "Daniel Caesar", "R&B", 12);
	yey.AddNode("loveAgain ", "Daniel Caesar", "R&B", 12);
	// Natty Dread
	yey.AddNode("Lively Up Yourself ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("No Woman No Cry ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Them Belly Full ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Rebel Music ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("So Jah Seh ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Natty Dread ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Bend Down Low ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Talkin Blues ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Ravolution ", "Bob Marley and the Wailers", "REGGAE", 11);
	//Legend
	yey.AddNode("Is This Love ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("No Woman No Cry ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Could You Be Loved ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Three Little Birds ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Buffalo Soldier ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Get Up Stand Up", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Stir It Up ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("One Love ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("I Shot The Sheriff ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Waiting In Vain ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Redemption Song ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Satisfy My Soul ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Exodus ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Jamming ", "Bob Marley and the Wailers", "REGGAE", 11);
	//Rastaman Vibrations
	yey.AddNode("Positive Vibrations ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Roots, Rocks, Reggae ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Johnny Was ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Cry To Me ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Want More ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Crazy Bald Head ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Who The Cap Fit", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Night Shift ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("War ", "Bob Marley and the Wailers", "REGGAE", 11);
	yey.AddNode("Rat Race ", "Bob Marley and the Wailers", "REGGAE", 11);
	//4 your Eyez only
	yey.AddNode("4 Your Eyez Only ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("Change ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("Deja Vu ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("Foldin Clothes ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("For Whom The Bells Toll ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("Immortal ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("Neighbors ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("She's Mine Pt. 5 ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("She's Mine Pt. 2 ", "JCOLE", "HIPHOP", 5);
	yey.AddNode("Ville Mentality ", "JCOLE", "HIPHOP", 5);
	//Russ The Mixtape
	yey.AddNode("For the Stunt ", "Russ", "HIPHOP", 5);
	yey.AddNode("Aint NobodyTakin My Baby ", "Russ", "HIPHOP", 5);
	yey.AddNode("2 A.M. ", "Russ", "HIPHOP", 5);
	yey.AddNode("Down For You ", "Russ", "HIPHOP", 5);
	yey.AddNode("Off The Strength ", "Russ", "HIPHOP", 5);
	yey.AddNode("We Just Havent Met Yet ", "Russ", "HIPHOP", 5);
	yey.AddNode("Always Knew", "Russ", "HIPHOP", 5);
	yey.AddNode("Whenever", "Russ", "HIPHOP", 5);
	yey.AddNode("Waste My Time", "Russ", "HIPHOP", 5);
	yey.AddNode("Inbetween", "Russ", "HIPHOP", 5);
	yey.AddNode("Inbetween", "Russ", "HIPHOP", 5);
	yey.AddNode("Lost", "Russ", "HIPHOP", 5);
	yey.AddNode("Yung God", "Russ", "HIPHOP", 5);
	yey.AddNode("Lately", "Russ", "HIPHOP", 5);
	yey.AddNode("Comin' Thru", "Russ", "HIPHOP", 5);
	yey.AddNode("Connected", "Russ", "HIPHOP", 5);
	yey.AddNode("Keep the Faith", "Russ", "HIPHOP", 5);
	yey.AddNode("All My Angels", "Russ", "HIPHOP", 5);
	yey.AddNode("Brush Me (feat. Colliee Buddz)", "Russ", "HIPHOP", 5);
	//Kupal Destroyerr 1
	yey.AddNode("Lamasin Ang Osus", "TUBERO", "METAL", 7);
	yey.AddNode("Animal ", "TUBERO", "METAL", 7);
	yey.AddNode("Coming ", "TUBERO", "METAL", 7);
	yey.AddNode("Makapal ang Blbl mo ", "TUBERO", "METAL", 7);
	yey.AddNode("Sir tngna mo pkyu ALbum VEr ", "TUBERO", "METAL", 7);
	yey.AddNode("Isubo ang ulo ", "TUBERO", "METAL", 7);
	yey.AddNode("Ptngna 2 ", "TUBERO", "METAL", 7);
	yey.AddNode("PtngnaTrapik ka ", "TUBERO", "METAL", 7);
	yey.AddNode("Sira Ulo ka ", "TUBERO", "METAL", 7);
	yey.AddNode("Ptngna 1 ", "TUBERO", "METAL", 7);
	yey.AddNode("Anak ka ng Pakyu Album Ver ", "TUBERO", "METAL", 7);
	yey.AddNode("Kupal Ka ", "TUBERO", "METAL", 7);
	yey.AddNode("Galit ako sa Gwapo ", "TUBERO", "METAL", 7);
	yey.AddNode("Mamatay ka na ", "TUBERO", "METAL", 7);
	yey.AddNode("Mamatay ka na sana ", "TUBERO", "METAL", 7);
	yey.AddNode("Gard Tngna mo ", "TUBERO", "METAL", 7);
	yey.AddNode("BAstos Ka ", "TUBERO", "METAL", 7);	
	yey.AddNode("Mamatay ka na sana Album VEr", "TUBERO", "METAL", 7);
	yey.AddNode("Sir Tanginamo Pakyuuu ", "TUBERO", "METAL", 7);
	//December Avenue (20110)
	yey.AddNode("City Lights ", "December Avenue", "ALTERNATIVE", 2);
	yey.AddNode("Sleep Tonight ", "December Avenue", "ALTERNATIVE", 2);
	yey.AddNode("Fallin� ", "December Avenue", "ALTERNATIVE", 2);
	yey.AddNode("Ears and Rhymes ", "December Avenue", "ALTERNATIVE", 2);
	yey.AddNode("I�ll Be Watching You ", "December Avenue", "ALTERNATIVE", 2);
	yey.AddNode("Back To Love ", "December Avenue", "ALTERNATIVE", 2);
	yey.AddNode("Eroplanong Papel ", "December Avenue", "ALTERNATIVE", 2);
	yey.AddNode("Dive ", "December Avenue", "ALTERNATIVE", 2);
	yey.AddNode("Forever ", "December Avenue", "ALTERNATIVE", 2);
	yey.AddNode("Breathe Again ", "December Avenue", "ALTERNATIVE", 2);
	yey.AddNode("Time To Go ", "December Avenue", "ALTERNATIVE", 2);
	//Too Weird To Live, Too Rare To Die!
	yey.AddNode("Casual Affair ", "Panic! At The Disco", "POP-ROCK", 10);
	yey.AddNode("Collar Full  ", "Panic! At The Disco", "POP-ROCK", 10);
	yey.AddNode("Girl That You Love ", "Panic! At The Disco", "POP-ROCK", 10);
	yey.AddNode("Miss Jackson ", "Panic! At The Disco", "POP-ROCK", 10);
	yey.AddNode("Nicotine ", "Panic! At The Disco", "POP-ROCK", 10);
	yey.AddNode("The End Of All Things ", "Panic! At The Disco", "POP-ROCK", 10);
	yey.AddNode("This Is Gospel ", "Panic! At The Disco", "POP-ROCK", 10);
	yey.AddNode("Vegas Lights ", "Panic! At The Disco", "POP-ROCK", 10);
	//Master Of Puppets
	yey.AddNode("Battery ", "METALLICA", "METAL", 7);
	yey.AddNode("Damage, Inc. ", "METALLICA", "METAL", 7);
	yey.AddNode("Disposable Heroes ", "METALLICA", "METAL", 7);
	yey.AddNode("Leper Messiah ", "METALLICA", "METAL", 7);
	yey.AddNode("Master Of Puppets ", "METALLICA", "METAL", 7);
	yey.AddNode("Orion ", "METALLICA", "METAL", 7);
	yey.AddNode("The Thing That Should Be ", "METALLICA", "METAL", 7);
	yey.AddNode("Welcome Home (Sanitarium) ", "METALLICA", "METAL", 7);
	//Get Your Heart On!
	wow.Push("Walang Kwenta", "Bassilyo", "HIPHOP", 5);
	yey.AddNode("You Suck at Love ", "Simple Plan", "POP-PUNK", 9);
	wow.Push("All of a Sudden","Matt Monro","COUNTRY",3);
	yey.AddNode("Can't Keep My Hands off You ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Jet Lag ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Astronaut ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Loser of the Year ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Anywhere Else But Here ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Freaking Me Out ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Summer Paradise ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Gone Too Soon ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Last One Standing ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("This Song Saved My Life ", "Simple Plan", "POP-PUNK", 9);
	//Still Not Getting Any
	yey.AddNode("Shut Up! ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Welcome to My Life ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Perfect World ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Thank You ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Me Against the World ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Jump ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Everytime ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Promise ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("One ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Untitled ", "Simple Plan", "POP-PUNK", 9);
	yey.AddNode("Perfect ", "Simple Plan", "POP-PUNK", 9);
	//Vessels
	yey.AddNode("Ode To sleep  ", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("Holding on to you  ", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("Migraine  ", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("House Of gold  ", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("car Radio  ", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("Semi Automatic  ", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("SCreen  ", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("The Run and Go  ", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("Fake You Out  ", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("Guns for Hands  ", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("Trees  ", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("Truce  ", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("Holding on to you  ", "Twenty One Pilots", "EMO", 4);
	//Blurry Face
	yey.AddNode("Heavy Dirty Soul  ", "Twenty One Pilots", "EMO",  4);
	yey.AddNode("Stressed Out  ", "Twenty One Pilots", "EMO",  4);
	yey.AddNode("Ride  ", "Twenty One Pilots", "EMO",  4);
	yey.AddNode("Fairly Local  ", "Twenty One Pilots", "EMO",  4);
	yey.AddNode("Tear in my heart  ", "Twenty One Pilots", "EMO",  4);
	yey.AddNode("Lane Boy  ", "Twenty One Pilots", "EMO",  4);
	yey.AddNode("The Judge  ", "Twenty One Pilots", "EMO",  4);
	yey.AddNode("Doubt  ", "Twenty One Pilots", "EMO",  4);
	yey.AddNode("Polarize  ", "Twenty One Pilots", "EMO",  4);
	yey.AddNode("We Dont Believe What's on TV  ", "Twenty One Pilots", "EMO",  4);
	yey.AddNode("Message Man  ", "Twenty One Pilots", "EMO",  4);
	yey.AddNode("Home Town  ", "Twenty One Pilots", "EMO",  4);
	yey.AddNode("Not Today", "Twenty One Pilots", "EMO", 4);
	yey.AddNode("Goner", "Twenty One Pilots", "ROCK",4);
	
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	cout<<"           MUSIC PLAYER INITIALIZING...\n";
	for(int i=10; i<=100; i+=15){
		system("pause");
		if(i==100){
			SetConsoleTextAttribute(hConsole, 7); 
			system("cls");
			cout<<i<<"%\n";
			cout<<"OPENING...\n";
			system("pause");
			system("cls");
		}
		SetConsoleTextAttribute(hConsole, i); 
		system("cls");
		cout<<i<<"%\n";
	}
	system("cls");
	SetConsoleTextAttribute(hConsole, 10); 
	cout<<"                     * ======================================================== * \n";
	cout<<"\n";
	cout<<"                    	      DATA STRUCTURES AND ALGORITHM FINAL PROJECT		  \n";
	cout<<"                    	            Created by: John Vincent S. Orolfo			  \n";
	cout<<"                   		      Passed to: Engr. Frank Chin			  \n";
	cout<<"\n";
	cout<<"                     * ======================================================== * \n";
	system("pause");
	intro();
	return 0;
}
